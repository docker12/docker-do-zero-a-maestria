CMD 
 - Runtime
 - Executa comandos em container durante a inicialização

RUN
 - Buildtime / Compilação
 - Compila e adiciona camadas às imagens
 - Usado para instalar aplicações

Shell Form
 - Os comandos são expressos da mesma forma que os comandos de shell 
 - Os comandos são Apendados com : "/bin/sh -c"

Exec Form
 - Json array style ["comand","arg1"]
 - Container não precisam de shell
 - Garante que a string não seja sobrescrita pelo Shell
 - Sem funcionalidades de Shell

ENTRYPOINT
 - Não pode ser sobrescrito em tempo de execução por comandos normais
	docker rn ... <comandos>
 - Quaquer comando em tempo de execução pode ser usado como um argumento no ENTRYPOINT
	docker run <argumentos> <comando>

