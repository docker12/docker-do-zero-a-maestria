Níveis de Log
 - Debug
     debug
     info
     warn
     error
 
 - Info
     info
     warn
     error
     fatal

 - Warn
     warn
     error
     fatal

 - Error
     error
     fatal

 - fatal
     fatal

cmd: dockerd -l info &
     - caso queira parar tem que matar o process -> ps -ef | grep docker
